import copy
from collections import defaultdict
from zope.interface import implements

from pyramid.renderers import render
from pyramid.security import effective_principals

from karl.utils import find_events
from karl.views.interfaces import IIntranetPortlet
from karl.content.views.adapters import AbstractPortlet


def file_base_url(url, endstr='/files/'):
    """drop everything after endstr in a URL"""
    try:
        new_len = url.index(endstr) + len(endstr)
    except ValueError:
        return
    return url[:new_len]


class ActivityItem(object):
    """enough details for the feed"""
    def __init__(self, activities):
        count = len(activities)
        event = activities[0]
        metadata = event[2]
        self.url = metadata['url']
        self.thumbnail = metadata['thumbnail']
        self.profile_url = metadata['profile_url']
        self.author = metadata['author']
        self.operation = metadata['operation']
        self.context_url = metadata['context_url']
        self.context_name = metadata['context_name']
        self.timeago = metadata['timestamp'].strftime('%Y-%m-%dT%H:%M:%SZ')
        if count == 1:
            self.count = 'a'
            self.content_type = metadata['content_type']
        else:
            self.count = str(count)
            # make the type plural
            etype = metadata['content_type']
            if etype.endswith('y'):
                self.content_type = etype[:-1] + 'ies'
            else:
                self.content_type = etype + 's'
            if etype == 'File':
                # link to the base URL when there are multiple files
                self.url = file_base_url(metadata['url'])


class LatestActivityPortlet(AbstractPortlet):
    """ use the content feeds to show the 5 most recently added items
    """
    implements(IIntranetPortlet)

    @property
    def title(self):
        return "Latest Activity"

    @property
    def entries(self):
        """ return 10 of the latest content feed items
        """
        principals = effective_principals(self.request)
        created_by = None
        activities = defaultdict(list)
        mentioned = set()
        events = find_events(self.context)
        site_events = events.checked(principals, created_by)

        ignore_operations = ['tagged', 'joined']
        ignore_types = ['Person', 'Wiki']
        while len(activities) < 10:
            try:
                event = site_events.next()
                operation = event[2]['operation']
                etype = event[2]['content_type']
                userid = event[2]['userid']
                day = event[2]['timestamp'].strftime('%Y-%m-%d')
                url = event[2]['url']
                if operation in ignore_operations or \
                   etype in ignore_types:
                    continue
                # skip attachments and inline images, collapse other uploads
                if etype == 'File':
                    # give multiple files just the base URL
                    # (same URL means same signature)
                    new_url = file_base_url(url)
                    signature = (userid, etype, operation, day, new_url)
                    if new_url is None:
                        # ignore files outside the 'Files' area
                        continue
                else:
                    signature = (userid, etype, operation, day, url)
                if url in mentioned:
                    if signature in activities:
                        # ignore identical events on the same item
                        # (multiple edits, most likely)
                        continue
                    if operation == 'added':
                        # look for edits on the same item and remove them
                        # the add will show instead
                        edit_sig = (userid, etype, 'edited', day, url)
                        edit_events = copy.copy(activities.get(edit_sig, []))
                        for edit_event in edit_events:
                            if edit_event[2]['url'] == url:
                                activities[edit_sig].remove(edit_event)
                        if len(activities.get(edit_sig, ['not empty'])) == 0:
                            # no other edits - drop this signature
                            del activities[edit_sig]
                    if operation == 'edited':
                        # look for an "add" of the same item
                        # if found, don't record the edits
                        add_sig = (userid, etype, 'added', day, url)
                        add_events = activities.get(add_sig, [])
                        skip_edit = False
                        for add_event in add_events:
                            if add_event[2]['url'] == url:
                                skip_edit = True
                                break
                        if skip_edit:
                            continue
                activities[signature].append(event)
                mentioned.add(url)
            except StopIteration:
                break

        # sort on the timestamp of the most recent activity
        sorted_activities = sorted(
            activities.values(),
            key=lambda a: a[0][2]['timestamp'],
            reverse=True,
        )
        return [ActivityItem(act) for act in sorted_activities]

    @property
    def asHTML(self):
        """ render our portlet
        """
        return render(
            'templates/latestactivity.pt',
            {'title': self.title,
             'latest_items': self.entries},
            request=self.request
            )
