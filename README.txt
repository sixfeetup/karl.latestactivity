.. contents::

Introduction
============

This is a simple product that turns your Karl Feeds tab into a portlet provider and allows you to add a portlet to your offices in the middle or right columns.
